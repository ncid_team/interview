﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Interview.Models;

namespace Interview.Controllers
{
    public class ChihuahuasController : Controller
    {
        private Context db = new Context();

        // GET: Chihuahuas
        public ActionResult Index()
        {
            return View(db.Chihuahuas.ToList());
        }

        // GET: Chihuahuas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chihuahua chihuahua = db.Chihuahuas.Find(id);
            if (chihuahua == null)
            {
                return HttpNotFound();
            }
            return View(chihuahua);
        }

        // GET: Chihuahuas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Chihuahuas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,AgeAtIntake,Description,IntakeDate,AdoptedDate")] Chihuahua chihuahua)
        {
            if (ModelState.IsValid)
            {
                db.Chihuahuas.Add(chihuahua);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(chihuahua);
        }

        // GET: Chihuahuas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chihuahua chihuahua = db.Chihuahuas.Find(id);
            if (chihuahua == null)
            {
                return HttpNotFound();
            }
            return View(chihuahua);
        }

        // POST: Chihuahuas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,AgeAtIntake,Description,IntakeDate,AdoptedDate")] Chihuahua chihuahua)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chihuahua).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(chihuahua);
        }

        // GET: Chihuahuas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chihuahua chihuahua = db.Chihuahuas.Find(id);
            if (chihuahua == null)
            {
                return HttpNotFound();
            }
            return View(chihuahua);
        }

        // POST: Chihuahuas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Chihuahua chihuahua = db.Chihuahuas.Find(id);
            db.Chihuahuas.Remove(chihuahua);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
