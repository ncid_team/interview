﻿namespace Interview.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChihuahuaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chihuahuas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        AgeAtIntake = c.Int(nullable: false),
                        Description = c.String(),
                        IntakeDate = c.DateTime(nullable: false),
                        AdoptedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            DropTable("dbo.Chihuahuas");
        }
    }
}
