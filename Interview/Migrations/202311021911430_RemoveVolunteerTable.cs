﻿namespace Interview.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveVolunteerTable : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Volunteers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Volunteers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
    }
}
