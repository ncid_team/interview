﻿using Interview.Models;

namespace Interview.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.IO;

    internal sealed class Configuration : DbMigrationsConfiguration<Context>
    {
        private static Random random = new Random();

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context context)
        {
            AddChihuahuasToContext(context, 1000);

            base.Seed(context);
        }

        private void AddChihuahuasToContext(Context context, int numberOfPets)
        {
            string[] chihuahuaNames = File.ReadAllLines(@"..\ChihuahuaNames.csv");
            string[] chihuahuaDescriptions = File.ReadAllLines(@"..\ChihuahuaDescriptions.csv");

            for(int i = 0; i < numberOfPets; i++)
            {
                context.Chihuahuas.Add(GenerateSingleChihuahua(chihuahuaNames, chihuahuaDescriptions));
            }

            context.SaveChanges();
        }

        private Chihuahua GenerateSingleChihuahua(string[] names, string[] descriptions)
        {
            Chihuahua chihuahua = new Chihuahua();

            chihuahua.Name = names[GetRandom(0, names.Length)];
            chihuahua.Description = descriptions[GetRandom(0, descriptions.Length)];
            chihuahua.AgeAtIntake = GetRandom(0, 15);

            DateTime startTime = new DateTime(2000, 1, 1);
            int dateDifference = (DateTime.Now - startTime).Days - 5;

            chihuahua.IntakeDate = startTime.AddDays(GetRandom(0, dateDifference));
            chihuahua.AdoptedDate = chihuahua.IntakeDate.AddDays(GetRandom(0, 600));

            if(chihuahua.AdoptedDate > DateTime.Now)
            {
                chihuahua.AdoptedDate = null;
            }

            return chihuahua;
        }

        private static int GetRandom(int min, int max)
        {
            return random.Next(min, max);
        }
    }
}
