﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Interview.Models
{
    public class Chihuahua
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public int AgeAtIntake { get; set; }

        public string Description { get; set; }

        public DateTime IntakeDate { get; set; }

        public DateTime? AdoptedDate { get; set; }
    }
}