﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Interview.Models
{
    public class Context : DbContext
    {
        public Context() : base("name=Interview"){}

        public DbSet<Chihuahua> Chihuahuas { get; set; }
    }
}